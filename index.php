<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $jenisanimal = new Animal("shaun");
    echo "Name : ".$jenisanimal->name."<br>";
    echo "legs : ".$jenisanimal->legs."<br>";
    echo "cold_blooded : ".$jenisanimal->cold_blooded."<br><br>";

    $Buduk = new buduk("Buduk");
    echo "Name : ".$Buduk->name."<br>";
    echo "legs : ".$Buduk->legs."<br>";
    echo "cold_blooded : ".$Buduk->cold_blooded."<br>";
    echo $Buduk->Jump();
    echo "<br><br>";

    $kera = new Ape("kera sakti");
    echo "Name : ".$kera->name."<br>";
    echo "legs : ".$kera->legs."<br>";
    echo "cold_blooded : ".$kera->cold_blooded."<br>";
    echo $kera->yell();
?>